#!/bin/bash

jml_zip=$(ls -d devil_* 2>/dev/null | wc -l)
zip_name="devil_$((jml_zip + 1))"

folder_count=$(ls -d kumpulan_* 2>/dev/null | wc -l)

if [ $(expr $folder_count % 5) == 3 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2 kumpulan_$folder_count3
elif [ $(expr $folder_count % 5) == 0 ]
then
    zip -r $zip_name kumpulan_$folder_count kumpulan_$folder_count2
fi


hour=$(date +"%H")
if [ "$hour" == 00 ]; then
    countX=24
else
    countX=1
fi

folder_count=$(ls -d kumpulan_* | wc -l)

if [ "$folder_count" -eq "0" ]; then
    nama_folder="kumpulan_1.FOLDER"
else
    next_number=$((folder_count + 1))
    nama_folder="kumpulan_${next_number}.FOLDER"
fi

mkdir -p "$nama_folder"

echo "file akan didownload sebanyak $countx kali"

index=1
while [ "$index" -le "$countx" ]; do
    name_file="perjalanan_$i.jpg"
    index=$((index+1))
    wget -O "$folder_name/$name_file" https://artikula.id/wp-content/uploads/2021/12/wayang.jpg
done
