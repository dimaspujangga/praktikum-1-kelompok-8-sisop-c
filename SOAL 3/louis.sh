#!/bin/bash

mkdir -p "Register_System"

log_file="Register_System/log.txt"

users_file="Register_System/users.txt"

function log {
	echo "$(date +'%Y/%m/%d %H:%M:%S')  'REGISTER:' $1 $2" >> "$log_file"
}


function check_username {
	if [[ "$(grep "$username" "$users_file")" ]]; then
	   echo -e "User already exist. Please try a different username!"
	   log "ERROR" "User already exist"
	fi
}

function check_password {
	if (( ${#password} < 8 )); then
	   echo "Password must be at least 8 characters long!"  
	elif [[ ! ${password} =~ [[:upper:]] || ! ${password} =~ [[:lower:]] ]]; then
	   echo -e "\nPassword must contain at least 1 uppercase letter 
	   and 1 lowercase letter!"
	elif echo "$password" | grep -q -v ^[[:alnum:]]*$; then 
	   echo -e "\nPassword must contain alphanumeric character!"
	elif [[ "$password" == "$username" ]]; then
	   echo -e "\nPassword cannot be the same as the username!"
	elif [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
	   echo -e "\nPassword cannot use word 'chicken' or 'ernie'"
	fi
	   
	   echo "$username:$password" >> "$users_file"
	   echo -e "\nAccount has been created!"
	   log "INFO" "User $username registered succesfully"  	   	
}

function main {
	echo "Family Guy"
	echo "Registration System"
	read -p "Please enter your username: " username
	read -sp "Please enter your password: " password 
	echo
}

main 

if check_username; then
   check_password
fi
